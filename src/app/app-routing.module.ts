import { ModuleWithProviders  } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'items',
    loadChildren: 'src/app/pages/search/search.module#SearchModule', //LAZY LOAD
    data: { preload: true },
  },
  {
    path: 'items/:id',
    loadChildren: 'src/app/pages/detail/detail.module#DetailModule', //LAZY LOAD
    data: { preload: true },
  },
  { path: '', redirectTo: '/', pathMatch: 'full' }

];


export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(routes);

