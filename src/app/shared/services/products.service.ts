import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { filter, map, retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  public category: any;
  draw: {
    name: 'Luis',
    lastname: 'Alvarez'
  }
  private _headers = new HttpHeaders().set('Content-Type', 'application/json');
  constructor(private http: HttpClient) { }

  searchProducts(query): any {
    const author = this._headers.append('author', 'Luis ' + 'Alvarez');
    return this.http.get(environment.apiURL + environment.region + 'search?q=' + query, { headers : author })
      .pipe(
        retry(3),
        map(res => {
          if (!res) {
            throw new Error('Value expected!');
          }
          return res;
        })
      )

  }

  getProduct(id) {
    const author = this._headers.append('author', 'Luis ' + 'Alvarez');
    return this.http.get(environment.apiURL + "items/" + id, { headers : author })
      .pipe(
        retry(3),
        map(res => {
          if (!res) {
            throw new Error('Value expected!');
          }
          return res;
        })
      )
  }

  getProductDescription(id) {
    const author = this._headers.append('author', 'Luis ' + 'Alvarez');
    return this.http.get(environment.apiURL + "items/" + id + "/description", { headers : author })
      .pipe(
        retry(3),
        map(res => {
          if (!res) {
            throw new Error('Value expected!');
          }
          return res;
        })
      )
  }

  getBreadcrumb(value) {
    value = value.available_filters;

    const category = value.filter(type => {
      return type.id === 'category';
    });
    if (category.length !== 0) {
      const allCategories = category.map(categories => {
        return categories.values;
      });
      const findMaxResults = allCategories[0].map(result => {
        return result;
      });
      const max = findMaxResults.reduce(function (prev, current) {
        return (prev.results > current.results) ? prev : current
      })
      return [max.name];
    }
  }
  saveCategory(category) {
    this.category = category;
    return this.category;
  }
}
