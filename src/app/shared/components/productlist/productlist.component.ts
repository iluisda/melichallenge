import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.scss']
})
export class ProductlistComponent implements OnInit {
  @Input('thumbnail') thumbnailproduct: string;
  @Input('price') priceproduct: string;
  @Input('title') titleproduct: string;
  @Input('location') locationproduct: string;
  @Input('currency') currencyproduct: string;
  @Input('shipping') shippingproduct: string;
  @Input('id') idproduct: string;
  @Output() link = new EventEmitter<any>();
  thumbnail: string;
  price: string;
  title: string;
  location: string;
  currency: string;
  shipping: string;
  id: string;
  constructor() { }

  ngOnInit() {
    this.thumbnail = this.thumbnailproduct;
    this.price = this.priceproduct;
    this.title = this.titleproduct;
    this.location = this.locationproduct;
    this.currency = this.currencyproduct;
    this.shipping = this.shippingproduct;
    this.id = this.idproduct;
  }
  goToProduct() {
   
    this.link.emit(this.id);
  }

}
