import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input('textobusqueda') texto: string;
  @Input('placeholder') textoPlaceholder: string;
  @Input('icon') iconheader: string;
  @Output() search = new EventEmitter<any>();
  textobusqueda: string;
  placeholder: string;
  icon: string;

  constructor(public router: Router) { }

  ngOnInit() {
    this.placeholder = this.textoPlaceholder;
    this.icon = this.iconheader;
  }
  emitEvent() {
    if(this.textobusqueda !== undefined) {
      this.router.navigate(['/items'], { queryParams: { search: this.textobusqueda } })
    }
  }
  goToHome() {
    this.clear();
    this.router.navigate(['/']);
  }
  clear() {
    this.textobusqueda = undefined;
    return this.textobusqueda;
  }
}
