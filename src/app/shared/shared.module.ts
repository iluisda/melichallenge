import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
// APLICATION COMPONENTS
import { HeaderComponent } from './components/header/header.component';
import { BreadcrumbComponent } from './components/breadcrumb/breadcrumb.component';
import { ProductlistComponent } from './components/productlist/productlist.component';

// APLICATION SERVIES
import { ProductsService } from './services/products.service';

@NgModule({
  declarations: [ 
    HeaderComponent,
    BreadcrumbComponent,
    ProductlistComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    BreadcrumbComponent,
    ProductlistComponent
  ]
})
export class SharedModule { 
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ProductsService]
    };
  }
}
