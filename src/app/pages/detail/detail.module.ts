import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailRouting } from './detail.routing';
import { DetailComponent } from './detail.component';
// import { HeroService } from './shared/hero.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DetailRouting
  ],
  providers: [
    // HeroService
  ],
  declarations: [DetailComponent],
  entryComponents: []
})
export class DetailModule { }
