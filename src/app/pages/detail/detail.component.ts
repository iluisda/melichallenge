import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../shared/services/products.service';
import { ActivatedRoute } from '@angular/router';
import { trigger, style, transition, animate, group } from '@angular/animations';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  animations: [
    trigger('detailproduct', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('1s ease-in', style({ opacity: '1' })),
      ]),
      transition(':leave', [
        group([
          animate('0.2s ease', style({
            transform: 'translate(150px,25px)'
          })),
          animate('0.5s 0.2s ease', style({
            opacity: 0
          }))
        ])
      ])
    ])
  ]
})
export class DetailComponent implements OnInit {
  id:any;
  product:any;
  description:any;
  category:any;
  details:object;
  constructor(private productService: ProductsService, private route: ActivatedRoute,) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
        this.id = params['id'];
    });
    if(this.id) {
      this.getProductDetails(this.id);
    }
  }
  getProductDetails(id){
    this.productService.getProduct(id).subscribe(res => {
      this.product = res;
      this.category = this.productService.category;
      this.productService.getProductDescription(this.product.id).subscribe(res => {
        this.description = res;
        this.details = {
          picture: this.product.pictures[0].url,
          condition: this.product.condition,
          sold_quantity: this.product.sold_quantity,
          title: this.product.title,
          currency_id: this.product.currency_id,
          price: this.product.price,
          description: this.description
        };
      })
    },
    err => {
      console.error('Oops:', err.message);
    },
    () => {
      console.log(`We're done here!`);
    });
  }

}
