import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRouting } from './search.routing';
import { SearchComponent } from './search.component';
// import { HeroService } from './shared/hero.service';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SearchRouting
  ],
  providers: [
    // HeroService
  ],
  declarations: [SearchComponent],
  entryComponents: []
})
export class SearchModule { }
