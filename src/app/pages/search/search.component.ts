import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../shared/services/products.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { trigger, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  animations: [
    trigger('listofproducts', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('1s ease-in', style({ opacity: '1' })),
      ])
    ])
  ]

})
export class SearchComponent implements OnInit {
  categories = [];
  listofproducts = [];
  search: any;
  constructor(private productService: ProductsService, private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {

    this.route.queryParamMap.subscribe(params => {
      this.search = {...params};
      this.getData(this.search.params.search);
    });

  }

  getData(query) {
    if(query !== '') {
      this.productService.searchProducts(query).subscribe(res => {
        this.listofproducts = res.results;
        this.categories = this.productService.getBreadcrumb(res);
      },
      err => {
        console.error('Oops:', err.message);
      },
      () => {
        console.log(`We're done here!`);
      });
    }
  }
  test(link){
    this.productService.saveCategory(this.categories);
    this.router.navigate(['/items', link]);
  }
}
